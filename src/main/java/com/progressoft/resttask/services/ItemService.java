package com.progressoft.resttask.services;

import com.progressoft.resttask.ItemModelAssembler;
import com.progressoft.resttask.ItemNotFoundException;
import com.progressoft.resttask.entities.Item;
import com.progressoft.resttask.entities.ItemRepository;
import org.springframework.hateoas.EntityModel;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ItemService {
    private final ItemModelAssembler assembler;

    private final ItemRepository repository;

    public ItemService(ItemRepository repository, ItemModelAssembler assembler) {
        this.repository = repository;
        this.assembler = assembler;
    }
    public EntityModel<Item> getItemById(@PathVariable int id) {
        Item item = repository.findById(id)
                .orElseThrow(() -> new ItemNotFoundException(id));
        return assembler.toModel(item);
    }
    public List<EntityModel<Item>> getAllItems() {
        return repository.findAll().stream()
                .map(assembler::toModel)
                .collect(Collectors.toList());
    }
    public EntityModel<Item> newItem(@RequestBody Item newItem) {

        return assembler.toModel(repository.save(newItem));
    }
    public EntityModel<Item> replaceItem(@RequestBody Item newItem) {
        Item updatedItem = repository.findById(newItem.getId())
                .map(item -> {
                    item.setName(newItem.getName());
                    return repository.save(item);
                })
                .orElseGet(() -> {
                    newItem.setId(newItem.getId());
                    return repository.save(newItem);
                });

        return assembler.toModel(updatedItem);
    }
    public void deleteItem(@PathVariable int id) {
        repository.deleteById(id);
    }

}
