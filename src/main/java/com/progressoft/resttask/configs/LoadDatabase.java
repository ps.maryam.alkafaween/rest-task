package com.progressoft.resttask.configs;

import com.progressoft.resttask.entities.Item;
import com.progressoft.resttask.entities.ItemRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class LoadDatabase {
    @Bean
    public String initDatabase(ItemRepository repository) {
        repository.save(new Item("Chair"));
        repository.save(new Item("Table"));
        return "Created";
    }
}
