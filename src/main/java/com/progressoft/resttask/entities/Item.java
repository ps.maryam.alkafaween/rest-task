package com.progressoft.resttask.entities;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Getter
@Setter
public class Item {

    @Id @GeneratedValue
    private int id;
    private String name;
    public Item(String name) {
        this.name = name;
    }

    public Item() {
        this("No Name");
    }
}
