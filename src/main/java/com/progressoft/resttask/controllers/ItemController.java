package com.progressoft.resttask.controllers;
import com.progressoft.resttask.services.ItemService;
import com.progressoft.resttask.entities.Item;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/items")
public class ItemController {
    private final ItemService service;
    public ItemController(ItemService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public EntityModel<Item> getItemById(@PathVariable int id) {
        return service.getItemById(id);
    }
    @GetMapping
    public CollectionModel<EntityModel<Item>> getAllItems() {
        List<EntityModel<Item>> items = service.getAllItems();
        return CollectionModel.of(items, linkTo(methodOn(ItemController.class).getAllItems()).withSelfRel());
    }
    @PostMapping
    public ResponseEntity<?> newItem(@RequestBody Item newItem) {
        EntityModel<Item> item = service.newItem(newItem);
        return ResponseEntity
                .created(item.getRequiredLink(IanaLinkRelations.SELF).toUri()) //
                .body(item);
    }
    @PutMapping
    public ResponseEntity<?> replaceItem(@RequestBody Item newItem) {
        EntityModel<Item> item = service.replaceItem(newItem);
        return ResponseEntity
                .created(item.getRequiredLink(IanaLinkRelations.SELF).toUri())
                .body(item);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteItem(@PathVariable int id) {
        service.deleteItem(id);
        return ResponseEntity.noContent().build();
    }

}
