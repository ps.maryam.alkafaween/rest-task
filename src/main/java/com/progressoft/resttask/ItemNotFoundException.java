package com.progressoft.resttask;

public class ItemNotFoundException extends RuntimeException{

    public ItemNotFoundException(int itemId) {
        super("Item with id " + itemId + " was not found");
    }
}
