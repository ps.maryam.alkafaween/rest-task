package com.progressoft.resttask.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.progressoft.resttask.ItemModelAssembler;
import com.progressoft.resttask.entities.Item;
import com.progressoft.resttask.services.ItemService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.hateoas.EntityModel;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ItemController.class)
@Import(TestConfiguration.class)
@ExtendWith(MockitoExtension.class)
class ItemControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private ItemService service;
    private ItemModelAssembler assembler;

    @BeforeEach
    void setUp() {
        openMocks(this);
        assembler = new ItemModelAssembler();
        Item item = new Item("sofa");
        Item item2 = new Item("chair");
        List<Item> itemsList = Arrays.asList(item, item2);
        List<EntityModel<Item>> items = itemsList.stream()
                .map(assembler::toModel)
                .collect(Collectors.toList());
        when(service.getItemById(anyInt())).thenReturn(assembler.toModel(item));
        when(service.getAllItems()).thenReturn(items);
        when(service.newItem(any(Item.class)))
                .then(invocation -> assembler.toModel(invocation.getArgument(0)));
        when(service.replaceItem(any(Item.class)))
                .then(invocation -> assembler.toModel(invocation.getArgument(0)));

    }

    @Test
    @SneakyThrows
    void whenPerformingGetForOneItem_thenStatusIsOkAndResultsMatchAndHyperlinkExists() {
        RequestBuilder builder = MockMvcRequestBuilders
                .get("/items/{id}", 1)
                .accept(APPLICATION_JSON);

        mockMvc.perform(builder)
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("_links.self.href").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("sofa"));
    }
    @Test
    @SneakyThrows
    void whenPerformingGetForAllItems_thenStatusIsOkAndResultsMatchAndHyperlinkExists() {
        RequestBuilder builder = MockMvcRequestBuilders
                .get("/items")
                .contentType(APPLICATION_JSON);

        mockMvc.perform(builder)
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("_links.self.href").exists())
                .andExpect(MockMvcResultMatchers.content().string(containsString("sofa")))
                .andExpect(MockMvcResultMatchers.content().string(containsString("chair")));
    }
    @Test
    @SneakyThrows
    void whenCreatingItem_thenStatusIsCreatedAndResultsMatchAndHyperlinkExists() {
        RequestBuilder builder = MockMvcRequestBuilders
                .post("/items")
                .contentType(APPLICATION_JSON)
                .content("{\n" +
                        "  \"name\": \"newItem\"\n" +
                        "}");

        mockMvc.perform(builder)
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("_links.self.href").exists())
                .andExpect(content().string(containsString("newItem")));
    }
    @Test
    @SneakyThrows
    void whenReplacingItem_thenStatusIsCreatedAndResultsMatchAndHyperlinkExists() {
        Item item = new Item("newSofa");
        RequestBuilder builder = MockMvcRequestBuilders
                .put("/items", 0)
                .contentType(APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(item));

        mockMvc.perform(builder)
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("_links.self.href").exists())
                .andExpect(content().string(containsString("newSofa")));
    }
    @Test
    @SneakyThrows
    void whenDeletingItem_thenStatusIsNoContent() {
        RequestBuilder builder = MockMvcRequestBuilders
                .delete("/items/{id}", 0)
                .contentType(APPLICATION_JSON);

        mockMvc.perform(builder)
                .andDo(print())
                .andExpect(status().isNoContent());
    }
}