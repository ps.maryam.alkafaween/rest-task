package com.progressoft.resttask;

import com.progressoft.resttask.controllers.ItemController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
class RestTaskApplicationTests {

	@Autowired
	private ItemController controller;

	@Test
	public void contextLoads() {
		assertThat(controller).isNotNull();
	}


}
